const fs = require('fs');
const csv = require('csv-parser');

let p1 =  process.argv[2] ? process.argv[2].toLocaleLowerCase() :  process.argv[2]
let p2 = process.argv[3] ? process.argv[3].toLocaleLowerCase() : process.argv[3]
let p3 = process.argv[4] ? process.argv[4].toLocaleLowerCase() : process.argv[4]

const csvData = [];
const palList = []
fs.createReadStream('./Palworld_Breeding_Table.csv')
    .pipe(csv({ 
        delimiter: ',',
        mapHeaders: (({ header }) => {
            if (header.charCodeAt(0) === 0xFEFF) {
                header = header.substr(1);
            }
            header = header.toLocaleLowerCase()
            return header;
        })
    }))
    .on('data', function(csvrow) {
        csvData.push(csvrow);
        palList.push(csvrow.name.toLocaleLowerCase())
    })
    .on('end', function() {
        if (p1 && p2 && p3 && p3 == 'partner') {
             csvData.forEach( (list) => {
                let existingComb = []
                for (const [key, value] of Object.entries(list)) {
                    if (value.toLocaleLowerCase() == p1 && key != 'name') {
                        if (list.name.toLocaleLowerCase() == p2 || key.toLocaleLowerCase() == p2) {
                            if (!existingComb.includes(list.name+key)) {
                                existingComb.push(list.name+key)
                                existingComb.push(key+list.name)
                                let keyColor = '\u001b[0m'
                                if (palList.indexOf(key.toLocaleLowerCase()) < 54) {
                                    keyColor = '\u001b[1;32m'
                                } else if (palList.indexOf(key.toLocaleLowerCase()) < 88) {
                                    keyColor = '\u001b[1;34m'
                                } else {
                                    keyColor = '\u001b[1;31m'
                                }
                                let nameColor = '\u001b[0m'
                                if (palList.indexOf(list.name.toLocaleLowerCase()) < 54) {
                                    nameColor = '\u001b[1;32m'
                                } else if (palList.indexOf(list.name.toLocaleLowerCase()) < 88) {
                                    nameColor = '\u001b[1;34m'
                                } else {
                                    nameColor = '\u001b[1;31m'
                                }
                                console.log('Peut être généré à partir de ' + keyColor + key + '\u001b[0m et ' + nameColor + list.name + '\u001b[0m')
                            }
                        } 
                    }
                  }
            }) 
        } else if (p1 && !palList.includes(p1)) {
            console.error('Invalid Pal!')
        }  else if (p1 && p2 && p2 == 'list') {
            let pal = csvData.find((l) => {
                return l.name.toLocaleLowerCase() == p1.toLocaleLowerCase()
            })
            let breedlist = []
            for (const [key, value] of Object.entries(pal)) {
                if( !breedlist.includes(value) && key != 'name') {
                    breedlist.push(value) 
                    if (palList.indexOf(value.toLocaleLowerCase()) < 54) {
                        console.log('Peut être accoupler avec ' + key + ' pour obtenir: \u001b[1;32m' + value + '\u001b[0m')
                    } else if (palList.indexOf(value.toLocaleLowerCase()) < 88) {
                        console.log('Peut être accoupler avec ' + key + ' pour obtenir: \u001b[1;34m' + value + '\u001b[0m')
                    } else {
                        console.log('Peut être accoupler avec ' + key + ' pour obtenir: \u001b[1;31m' + value + '\u001b[0m')
                    }
                }
            }
        } else if (p1 && p2) {
            let pal1 = csvData.find((l) => {
                return l.name.toLocaleLowerCase() == p1.toLocaleLowerCase()
            })
            console.log('L\'accouplement de ces pals produira: ' + pal1[p2])
        } else if (p1 && !p2) {
            csvData.forEach( (list) => {
                let existingComb = []
                for (const [key, value] of Object.entries(list)) {
                    if (value.toLocaleLowerCase() == p1 && key != 'name') {
                        if (!existingComb.includes(list.name+key)) {
                            existingComb.push(list.name+key)
                            existingComb.push(key+list.name)
                            let keyColor = '\u001b[0m'
                            if (palList.indexOf(key.toLocaleLowerCase()) < 54) {
                                keyColor = '\u001b[1;32m'
                            } else if (palList.indexOf(key.toLocaleLowerCase()) < 88) {
                                keyColor = '\u001b[1;34m'
                            } else {
                                keyColor = '\u001b[1;31m'
                            }
                            let nameColor = '\u001b[0m'
                            if (palList.indexOf(list.name.toLocaleLowerCase()) < 54) {
                                nameColor = '\u001b[1;32m'
                            } else if (palList.indexOf(list.name.toLocaleLowerCase()) < 88) {
                                nameColor = '\u001b[1;34m'
                            } else {
                                nameColor = '\u001b[1;31m'
                            }
                            console.log('Peut être généré à partir de ' + keyColor + key + '\u001b[0m et ' + nameColor + list.name + '\u001b[0m')
                        }
                    }
                  }
            }) 
        }
    });
