Retourne la liste des combinaisons pour créer un pal

``` npm run breed {pal1}```

Retourne la liste des oeuf pouvant être créé à partir du pal et avec quel partenaire

``` npm run breed {pal1} list```

Retourne l'oeuf créé par la combinaison des deux

```  npm run breed {pal1} {pal2}```

Retourne les combinaison possible pour créé le pal1 en tenant compte du pal2 comme parent

```  npm run breed {pal1} {pal2} partner```